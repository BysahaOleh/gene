import React from 'react';
import { render } from 'react-dom';
import Routes from './routes';
require('es6-promise').polyfill();

import './app.styl';

render(Routes, document.getElementById('app'));
