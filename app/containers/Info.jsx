import React, {Component, PropTypes} from 'react';
import  { connect } from 'react-redux';
import { Link } from 'react-router';

import './styles/info.styl';

const propTypes = {
    gene: PropTypes.shape({
        isFetching: PropTypes.bool,
        didInvalidate: PropTypes.bool,
        data: PropTypes.oneOfType([
            PropTypes.object,
            PropTypes.null
        ])
    }),
    params: PropTypes.shape({
        id: PropTypes.oneOfType([
            PropTypes.string,
            PropTypes.null
        ])
    })
};

const defaultProps = {
    gene: {
        isFetching: false,
        didInvalidate: false,
        data: null
    },
    params: {
        id: null
    }
};

export class Info extends Component {
    render() {
        if(this.props.gene.data === null) {
            return (
                <div id="result-list">
                    <h1>Not results</h1>
                    <Link className="button-base" to="/">Home</Link>
                </div>
            )
        }
        let content = this.props.gene.data.info[this.props.params.id];
        console.log(content);
        return (
            <div id="info">
                <h3>{this.props.params.id}</h3>
                <div className="info-body">
                    {
                        content['genome-type'].map( e => {
                            let state;
                            switch(e.warning) {
                                case true:
                                    state = ' warning';
                                    break;
                                case false:
                                    state = ' green';
                                    break;
                                default:
                                    state = '';
                            }
                            return (
                                <div className={'genome-type' + state}>
                                    <span className="geno">{e.geno}</span>
                                    <span className="mag">{e.mag}</span>
                                    <span className="summary">{e.summary}</span>
                                </div>
                            )
                        })
                    }
                    <div className="description">
                        {content.info}
                    </div>
                    <div className="link-block">
                        <a target="_blank" href={content.url}>More...</a>
                    </div>
                </div>
            </div>
        )
    }
}

Info.propTypes = propTypes;
Info.defaultProps = defaultProps;

export function mapStateToProps({ routing, gene }) {
    return {
        routing,
        gene
    }
}

export default connect(mapStateToProps)(Info);
