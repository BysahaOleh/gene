import React, {Component, PropTypes} from 'react';
import { connect } from 'react-redux';
import logoImage from '../img/logo.png';
import './styles/app.styl';
import { UploadButton } from '../components/Buttons';
import mock from '../other.json';
import deleteIcon from './image/delete.svg';
import { Link } from 'react-router';

console.log(mock);

import { fetchGene, receiveGene } from '../actions/gene';

const propTypes = {
    children: PropTypes.oneOfType([
        PropTypes.element,
        PropTypes.string,
        PropTypes.array
    ]),
    location: PropTypes.shape({
        pathname: PropTypes.string
    }),
    gene: PropTypes.shape({
        isFetching: PropTypes.bool,
        didInvalidate: PropTypes.bool,
        data: PropTypes.oneOfType([
            PropTypes.object,
            PropTypes.null
        ])
    }),
    dispatch: PropTypes.func
};

const defaultProps = {
    children: '',
    gene: {
        isFetching: false,
        didInvalidate: false,
        data: null
    },
    dispatch: ()=>{}
};

export class App extends Component {

    constructor(props) {
        super(props);

        this.state = {
            ready: false,
            modal: false
        };

        this.startPage = this.startPage.bind(this);
        this.readyPage = this.readyPage.bind(this);
        this.uploadFile = this.uploadFile.bind(this);
        this.uploadFileServer = this.uploadFileServer.bind(this);
        this.animationRouting = this.animationRouting.bind(this);
        this.fetchServer = this.fetchServer.bind(this);
        this.errorPage = this.errorPage.bind(this);
        this.goToList = this.goToList.bind(this);
        this.loadPage = this.loadPage.bind(this);
        this.toggleModal = this.toggleModal.bind(this);
        this.clearLocalStorage = this.clearLocalStorage.bind(this);
    }

    //Animation
    componentDidMount() {
        setTimeout(() => { this.animationRouting(this.props.location.pathname)}, 1500);
    }

    componentWillUpdate(nextProps, nextState) {
        let locationPath = this.props.location.pathname;
        let nextlocationPath = nextProps.location.pathname;

        if(locationPath !== nextlocationPath) {
            this.animationRouting(nextlocationPath);
        }
        if(nextProps.gene.isFetching) {
            this.setState({
                ready: false
            });
        }
        if(nextProps.gene.didInvalidate) {
            this.setState({
                ready: 'invalid'
            });
        }
    }

    fetchServer(params) {
        this.props.dispatch(fetchGene(params));
    }

    animationRouting(location) {
        let locationPath = location;

        let start = this.startPage;
        let ready = this.readyPage;

        switch(locationPath) {
            case '/':
                start();
                break;

            default:
                ready();
                break;
        }
    }

    //State view
    startPage() {
        this.setState({
            ready: 'welcome'
        })
    }
    readyPage() {
        this.setState({
            ready: 'ready'
        })
    }
    errorPage() {
        this.setState({
            ready: 'error'
        })
    }
    loadPage() {
        this.setState({
            ready: false
        })
    }

    receiveGene(json) {
        this.props.dispatch(receiveGene(json));
    }

    //Route
    goToList() {
        setTimeout(() => {
            this.receiveGene(mock);
            document.location.href = document.location.href + 'list';
        }, 5000)
    }

    //API
    uploadFileServer(f) {
        let fetch = this.fetchServer;

        if (f) {
            let r = new FileReader();
            r.onload = function (e) {
                fetch({
                    file: e.target.result
                })
            };
            r.readAsText(f);
        } else {
            console.log('Failed to load file');
        }
        this.goToList();

    }
    uploadFile(f) {
        this.loadPage();
        if (f) {
            if(f.name == 'genome_Walter_Wolfsberger.txt') {
                this.goToList();
            }
            else {
                setTimeout(this.errorPage, 3000);
            }
        } else {
            console.log('Failed to load file');
        }

    }
    clearLocalStorage() {
        localStorage.clear();
        this.receiveGene(null);
        this.closeModal();
    }
    toggleModal() {
        this.setState({
            modal: !this.state.modal
        })
    }
    closeModal() {
        this.setState({
            modal: false
        })
    }
    openModal() {
        this.setState({
            modal: true
        })
    }

    //Classes
    makeClasses() {
        let classes = ['app'];

        switch(this.state.ready) {
            case 'welcome':
                classes.push('welcome');
                break;

            case 'ready':
                classes.push('ready');
                break;
            case 'error':
                classes.push('error');
                break;
        }

        return classes.join(' ');
    }

    renderButton() {
        if(this.props.gene.data === null) {
            return (
                <UploadButton
                    className="gene-upload-button"
                    onChange={this.uploadFile}
                >Upload file
                </UploadButton>
            )
        } else {
            return (
                <Link
                    className="list-button"
                    to="list"
                >View list
                </Link>
            )
        }
    }

    render() {
        return (
            <div className={this.makeClasses()}>
                <img className="welcome-logo"src={logoImage}/>
                <h1 className="brand-name">GENE</h1>
                {this.renderButton()}
                <div className="children-block">
                    {this.props.children}
                </div>
                <div className="error-block">
                    File incorrect!
                </div>
                <div className={'clear-store' + (this.props.gene.data ? ' active' : '') }>
                    <img src={deleteIcon} onClick={this.toggleModal}/>
                    <div className={'modal' + (this.state.modal ? ' open' : '')} >
                        <span className="text">You want to remove your data</span>
                        <span className="cancel" onClick={this.toggleModal}>Cancel</span>
                        <spn className="yes"  onClick={this.clearLocalStorage}>Yes</spn>
                    </div>
                </div>
            </div>
        )
    }
}

App.propTypes = propTypes;
App.defaultProps = defaultProps;

export function mapStateToProps({ routing, gene }) {
    return {
        routing,
        gene
    }
}

export default connect(mapStateToProps)(App);
