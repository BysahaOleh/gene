import React, {Component, PropTypes} from 'react';
import { connect } from 'react-redux';
import './styles/result-list.styl';
import { Link } from 'react-router';

import { List, ListItem } from '../components/GeneList';

const propTypes = {
    gene: PropTypes.shape({
        isFetching: PropTypes.bool,
        didInvalidate: PropTypes.bool,
        data: PropTypes.oneOfType([
            PropTypes.object,
            PropTypes.null
        ])
    })
};

const defaultProps = {
    gene: {
        isFetching: false,
        didInvalidate: false,
        data: null
    }
};

export class ResultList extends Component {
    constructor(props) {
        super(props);

        this.renderListResult = this.renderListResult.bind(this);
    }
    renderListResult(genome) {
        return genome.map( e => {
            return (
                <ListItem
                    key={e.id}
                    id={e.id}
                    title={e.description}
                    geno={e.genotype}
                    mag={e.mag}
                    addiction={e.warning}
                />
            )
        })
    }
    render() {

        if(this.props.gene.data === null) {
            return (
                <div id="result-list">
                    <h1>Not results</h1>
                    <Link className="button-base" to="/">Home</Link>
                </div>
            )
        }
        return (
            <div id="result-list">
                <h1>Your result</h1>
                <List>
                    {this.renderListResult(this.props.gene.data.genome)}
                </List>
            </div>
        )
    }
}

export function mapStateToProps({routing, gene}) {
    return {
        routing,
        gene
    }

}

ResultList.propTypes = propTypes;
ResultList.defaultProps = defaultProps;

export default connect(mapStateToProps)(ResultList);
