import thunkMiddleware from 'redux-thunk';
import { createStore, applyMiddleware, compose } from 'redux';
import rootReducer from './reducers';
import persistState from 'redux-localstorage'

const store = createStore(
    rootReducer,
    compose(
        applyMiddleware(thunkMiddleware),
        persistState(['gene']),
        typeof window === 'object' &&
        typeof window.devToolsExtension !== 'undefined' ? window.devToolsExtension() : f => f
    )
);

export default store;
