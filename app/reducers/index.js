import { combineReducers } from 'redux';
import { syncHistoryWithStore, routerReducer } from 'react-router-redux';

import gene from './gene';

const rootReducer = combineReducers({
    routing: routerReducer,
    gene
});

export default rootReducer;
