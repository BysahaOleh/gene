import { RECEIVE_GENE, REQUEST_GENE, INVALIDATE_GENE } from '../actions/gene';

function gene(state = {
    isFetching: false,
    didInvalidate: false,
    data: null
}, action) {
    switch(action.type) {
        case INVALIDATE_GENE:
            return Object.assign({}, state, {
                isFetching: false,
                didInvalidate: true
            });
        case REQUEST_GENE:
            return Object.assign({}, state, {
                isFetching: true,
                didInvalidate: false
            });
        case RECEIVE_GENE:
            return Object.assign({}, state, {
                isFetching: false,
                didInvalidate: false,
                data: action.json
            });
        default:
            return state;
    }
}

export default gene;
