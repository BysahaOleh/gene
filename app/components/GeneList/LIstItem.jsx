import React, {Component, PropTypes} from 'react';
import { Link } from 'react-router';

import './list-item.styl';

const propTypes = {
    id: PropTypes.string,
    title: PropTypes.string,
    addiction: PropTypes.bool,
    geno: PropTypes.string,
    mag: PropTypes.string,
    description: PropTypes.string,
    onClick: PropTypes.func
};

const defaultProps = {
    id: '',
    title: '',
    addiction: false,
    deno: '',
    mag: '',
    description: '',
    onClick: e=>{}
};

export default class ListItem extends Component {
    render() {
        return (
            <Link className="list-item" to={'info/' + this.props.id}>
                <span className="id">{this.props.id}</span>
                <span className="title">{this.props.title}</span>
                <div className={'addition' + (this.props.addiction ? ' true' : '')}>
                    <span className="geno">{this.props.geno}</span>
                    <span className="mag">{this.props.mag}</span>
                </div>
            </Link>
        )
    }
}

ListItem.propTypes = propTypes;
ListItem.defaultProps = defaultProps;
