import React, {Component, PropTypes} from 'react';

import './list.styl';

const propTypes = {
    children: PropTypes.oneOfType([
        PropTypes.string,
        PropTypes.element,
        PropTypes.array
    ])
};

const defaultProps = {
    children: ''
};

export default class List extends Component {
    render() {
        return (
            <div className="list-component">
                {this.props.children}
            </div>
        )
    }
}

List.propTypes = propTypes;
List.defaultProps = defaultProps;
