import React, {Component, PropTypes} from 'react';

import './upload-button.styl';

const propTypes = {
    children: PropTypes.oneOfType([
        PropTypes.element,
        PropTypes.string,
        PropTypes.array
    ]),
    onChange: PropTypes.func,
    className: PropTypes.string
};

const defaultProps = {
    children: '',
    onChange: e=>{},
    className: ''
};

export default class ProgressButton extends Component {
    constructor(props) {
        super(props);

        this.state = {
            status: true
        };

        this.onChangeFile = this.onChangeFile.bind(this);
    }
    makeClasses() {
        let classes = ['upload-button-component', this.props.className];
        return classes.join(' ');
    }
    onChangeFile(evt) {
        let file = evt.target.files[0];

        this.props.onChange(file);
    }
    render() {
        return (
            <div className={this.makeClasses()}>
                <label className='upload-button' htmlFor="file-upload-button">
                    <span>{this.props.children}</span>
                </label>
                <input id="file-upload-button" type="file" onChange={this.onChangeFile}/>
            </div>

        )
    }
}

ProgressButton.propTypes = propTypes;
ProgressButton.defaultProps = defaultProps;
