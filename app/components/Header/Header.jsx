import React, {Component, PropTypes} from 'react';
import './header.styl';

import defaultImage from './logo.png';

const propTypes = {
    logoImg: PropTypes.string,
    title: PropTypes.string
};

const defaultProps = {
    logoImg: defaultImage,
    title: 'App'
};

export default class Header extends Component {
    render() {
        return (
            <header className="app-header">
                <img className="logo" src={this.props.logoImg}/>
                <h2 className="title">{this.props.title}</h2>
            </header>
        )
    }
}

Header.propTypes = propTypes;
Header.defaultProps = defaultProps;
