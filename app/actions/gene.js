const superagentPromisePlugin = require('superagent-promise-plugin');
const request = superagentPromisePlugin.patch(require('superagent'));

import { GENE } from '../constants';

export const REQUEST_GENE = 'REQUEST_GENE';
export function requestGene() {
    return {
        type: REQUEST_GENE
    }
}

export const RECEIVE_GENE = 'RECEIVE_GENE';
export function receiveGene(json) {
    return {
        type: RECEIVE_GENE, json
    }
}

export const INVALIDATE_GENE = 'INVALIDATE_GENE';
export function invalidateGene(err) {
    return {
        type: INVALIDATE_GENE, err
    }
}

export function fetchGene() {
    return dispatch => {
        dispatch(requestGene());
        return request.get(GENE)
            .then(response => {
                return dispatch(receiveGene(JSON.parse(response.text)))
            })
            .catch(err => dispatch(invalidateGene(err)))
    }
}
