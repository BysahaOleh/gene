import React from 'react';
import { Router, Route, IndexRedirect, useRouterHistory} from 'react-router';
import { createHashHistory } from 'history';
import store from './store';
import { Provider } from 'react-redux';
import { syncHistoryWithStore } from 'react-router-redux';

import App from './containers/App';
import ResultList from './containers/ResultList';
import Info from './containers/Info';

const appHistory = useRouterHistory(createHashHistory)({ queryKey: false });
const history = syncHistoryWithStore(appHistory, store);

export default (
    <Provider store={store}>
        <Router history={history} >
            <Route path="/" component={App}>
                <Route path="list" component={ResultList}/>
                <Route path="info/:id" component={Info}/>
            </Route>
        </Router>
    </Provider>
)
