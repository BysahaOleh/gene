var webpack = require('webpack');
var HtmlWebpackPlugin = require('html-webpack-plugin');
var ExtractTextPlugin = require('extract-text-webpack-plugin');

module.exports = {
    entry: './app/index',

    output: {
        path: __dirname + '/www',
        publicPath: '',
        filename: 'app.js'
    },

    module: {
        loaders: [
            {
                test: /\.(js|jsx)?$/,
                exclude: /(node_modules|bower_components)/,
                loader: 'babel?presets[]=react,presets[]=es2015,presets[]=stage-0'
            },
            {
                test: /\.css$/,
                loader: ExtractTextPlugin.extract('style', 'css?minimize')
            },
            {
                test: /\.styl/,
                loader: ExtractTextPlugin.extract('style','css!stylus?resolve url')
            },
            {
                test: /\.(png|jpg|gif|svg|ttf|eot|woff|woff2)/,
                include: /\/node_modules\//,
                loader: 'file?name=[1]?[hash]&regExp=node_modules/(.*)'
            },
            {
                test: /\.(png|jpg|gif|svg|ttf|eot|woff|woff2)/,
                exclude: /\/node_modules\//,
                loader: 'file?name=[path][name].[ext]?[hash]'
            },
            {
                test: /\.json/,
                exclude: /\/node_modules\//,
                loader: 'json'
            }
        ]
    },

    resolve: {
        extensions: ['', '.js', '.jsx'],
        modulesDirectories: ['app', 'node_modules']
    },

    plugins: [
        new webpack.NoErrorsPlugin(),
        new ExtractTextPlugin('styles.css'),
        new webpack.optimize.UglifyJsPlugin({
            compress: {
                warnings: false,
                drop_console: true,
                unsafe: true
            }
        }),
        new HtmlWebpackPlugin({
            template: './app/index.html',
            inject: 'body'
        })
    ]
};
