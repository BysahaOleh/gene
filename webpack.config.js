var webpack = require('webpack');
const HtmlWebpackPlugin = require('html-webpack-plugin');

module.exports = {
    entry: [
        'webpack-dev-server/client?http://localhost:4000',
        'webpack/hot/only-dev-server',
        './app/index'
    ],

    output: {
        path: __dirname + '/www',
        publicPath: '/',
        filename: 'app.js'
    },

    devtool: 'cheap-module-inline-source-map',

    module: {
        loaders: [
            {
                test: /\.(js|jsx)$/,
                exclude: /(node_modules|bower_components)/,
                loader: 'react-hot!babel?presets[]=react,presets[]=es2015,presets[]=stage-0'
            },
            {
                test: /\.css$/,
                loader: 'style!css?sourceMap'
            },
            {
                test: /\.styl$/,
                loader: 'style!css!stylus?resolve url'
            },
            {
                test: /\.(png|jpg|gif|svg|ttf|eot|woff|woff2)/,
                include: /\/node_modules\//,
                loader: 'url?name=[1].[ext]?[hash]&regExp=node_modules/(.*)?limite=4096'
            },
            {
                test: /\.(png|jpg|gif|svg|ttf|eot|woff|woff2)/,
                exclude: /\/node_modules\//,
                loader: 'url?name=[path][name].[ext]?[hash]?limite=4096'
            },
            {
                test: /\.json/,
                exclude: /\/node_modules\//,
                loader: 'json'
            }
        ]
    },

    resolve: {
        extensions: ['', '.js', '.jsx'],
        modulesDirectories: ['app', 'node_modules']
    },

    plugins: [
        new webpack.NoErrorsPlugin(),
        new webpack.HotModuleReplacementPlugin(),
        new HtmlWebpackPlugin({
            template: './app/index.html',
            inject: 'body'
        })
    ]
};